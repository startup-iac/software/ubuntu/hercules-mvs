#!/bin/bash

# More info:
# https://www.prince-webdesign.nl/index.php/software/mvs-3-8j-turnkey-5
# https://wotho.pebble-beach.ch/tk4-/

MVS_HOME="$HOME/mvs"

# Install unzip and Terminal c3270
sudo apt-get install -y unzip c3270

# Install Hercules and MVS
wget --no-check-certificate https://wotho.pebble-beach.ch/tk4-/tk4-_v1.00_current.zip -O /tmp/mvs-tk4.zip

unzip /tmp/mvs-tk4.zip -d $MVS_HOME

chmod -R +x $MVS_HOME/*

$MVS_HOME/unattended/set_console_mode

#wget https://www.prince-webdesign.nl/images/downloads/mvs-tk5.zip -O /tmp/mvs-tk5.zip
#unzip /tmp/mvs-tk5.zip -d $MVS_HOME
#chmod -R +x $MVS_HOME/mvs-tk5/*